# Dockercoins in ArgoCD

## Installer un cluster k3d 

```bash 
k3d cluster create demo --agents=3 -p "80:80@loadbalancer" -p "443:443@loadbalancer"
```

## Deployer dockercoins manuellement

1. Création du namespace : 

```bash
kubectl create ns dockercoins 
```

2. Création des déploiements : 

```bash 
kubectl create deploy redis --image=redis -n dockercoins 
kubectl create deploy hasher --image=dockercoins/hasher:v0.1 -n dockercoins
kubectl create deploy worker --image=dockercoins/worker:v0.1 -n dockercoins 
kubectl create deploy rng --image=dockercoins/rng:v0.1 -n dockercoins 
kubectl create deploy webui --image=dockercoins/webui:v0.1 -n dockercoins 
```

3. Vérifier que les déploiements sont prêts : 

```bash 
kubectl get deploy -n dockercoins -o wide  
```

4. Création des services 

```bash 
kubectl expose deploy redis --port=6379 -n dockercoins 
kubectl expose deploy hasher --port=80 -n dockercoins 
kubectl expose deploy rng --port=80 -n dockercoins 
kubectl expose deploy webui --port=80 -n dockercoins 
```

5. Exposer le service 

```bash 
kubectl port-forward svc/webui 8081:80 -n dockercoins
```

6. Créer un ingress 

```bash 
kubectl create ingress dockercoins --rule="dockercoins.net/*=webui:80" -n dockercoins  
```

## Deployer dockercoins avec ArgoCD 

1. Installer ArgoCD

```bash 
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
```

2. Récupérer le mot de passe admin 

```bash 
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d  
```

3. Exposer le service : 

```bash 
kubectl port-forward svc/argocd-server 8081:443 -n argocd
```

4. Créer l'application dans l'ui d'argo 

## Deployer dockercoins avec ArgoCD en utilisant le app-of-apps pattern

```bash 
kubectl apply -f namespace.yml
kubectl apply -f project.yml
kubectl apply -f app.yml
```
